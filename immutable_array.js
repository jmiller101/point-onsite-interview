"use strict";

/*
 * Immutable data cannot be changed once created.
 *
 * Write a simple wrapper function `immutableArray` that takes a `String` or
 * `Number` array and returns a new immutable array. Your immutable array must
 * have the following methods or properties:
 *
 * 1. `push(value)`: Add an element to the end of the array and returns a new
 *                   immutable array
 * 2. `pop()`: Remove the last element from the array and returns a new
 *             immutable array
 * 3. `get(index)`: Get the value at an index
 * 4. `length()`: Return the number of elements in the array
 */

function ImmutableArray(array) {
  const _array = array;

  return {
    array: () => {
      return _array;
    },

    push: (item) => {
      return ImmutableArray([..._array, item]);
    },

    pop: () => {
      return ImmutableArray([..._array.slice(0, _array.length - 1)]);
    },

    get: (index) => {
      return _array[index];
    },

    length: () => {
      return _array.length;
    },
  };
}

const test = [1, 2];
console.log(ImmutableArray(test).push(7).array());
console.log(ImmutableArray(test).pop().array());
console.log(ImmutableArray(test).length());
console.log(ImmutableArray(test).get(0));

console.log(ImmutableArray(test).get(-1));
console.log(ImmutableArray(test).pop().pop().pop().array());
