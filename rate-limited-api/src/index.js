/* Fetch data
 * @param {number} [page] - Page of data to return, optional
 * @returns {Promise} Promise Object - Payload of the request
 */

const { getData } = require("./api");

/*
 * Working with legacy and unreliable APIs is a common problem we have to
 * solve for at Point. For this challenge you must successfully fetch all pages from
 * an external API and sum all of the pages of data to a single integer value.
 * An example successful response from the API is:
 *
    {
      "data": [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150],
      "page": 0,
      "pageCount": 15
    }
 *
 * Things to watch out for:
 *  1. The API is rated limited and only allows 5 requests per 3 seconds
 *  2. The API is unreliable and has been known to fail randomly
 *  3. You can only use core NodeJS modules
 *
 *  Good luck!
 */

async function main() {
  let sum = 0;

  let currentPage = 0;
  let backoff = [0, 1, 2, 5];
  let currentBackoff = 0;
  while (true) {
    console.log("\n\n");
    console.log(currentPage);
    console.log(sum);

    let resp;
    try {
      await sleep(backoff[currentBackoff]);
      resp = await getData(currentPage);
      currentBackoff = 0;
    } catch (e) {
      console.log("error hit");
      if (e.statusCode == 429 && currentBackoff < backoff.length - 1) {
        currentBackoff++;
      }
      continue;
    }

    for (let d of resp.data) {
      sum += d;
    }
    if (currentPage >= resp.pageCount - 1) break;
    currentPage++;
  }

  console.log(`The total sum is: ${sum}`);
}

function sleep(seconds) {
  if (!seconds) return Promise.resolve();

  return new Promise((resolve) =>
    setTimeout(() => {
      resolve();
    }, seconds * 1000)
  );
}

main();
